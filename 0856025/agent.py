#!/usr/bin/env python3

"""
Basic framework for developing 2048 programs in Python

Author: Hung Guei (moporgic)
        Computer Games and Intelligence (CGI) Lab, NCTU, Taiwan
        http://www.aigames.nctu.edu.tw
"""

TILE_CONUT = 15

from board import board
from action import action
from weight import weight
from array import array
import random
import sys

class agent:
    """ base agent """

    def __init__(self, options = ""):
        self.info = {}
        options = "name=unknown role=unknown " + options
        for option in options.split():
            data = option.split("=", 1) + [True]
            self.info[data[0]] = data[1]
        return

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        return

    def open_episode(self, flag = ""):
        return

    def close_episode(self, flag = ""):
        return

    def take_action(self, state):
        return action()

    def check_for_win(self, state):
        return False

    def property(self, key):
        return self.info[key] if key in self.info else None

    def notify(self, message):
        data = message.split("=", 1) + [True]
        self.info[data[0]] = data[1]
        return

    def name(self):
        return self.property("name")

    def role(self):
        return self.property("role")

class random_agent(agent):
    """ base agent for agents with random behavior """

    def __init__(self, options = ""):
        super().__init__(options)
        seed = self.property("seed")
        if seed is not None:
            random.seed(int(seed))
        return

    def choice(self, seq):
        target = random.choice(seq)
        return target

    def shuffle(self, seq):
        random.shuffle(seq)
        return

class weight_agent(agent):
    """ base agent for agents with weight tables """

    def __init__(self, load_path, tuple_count, n_tuple):
        super().__init__()
        self.net = []
        if load_path == None:
            for i in range(tuple_count):
                self.net += [weight(TILE_CONUT ** n_tuple)]
        else:
            self.load_weights(load_path)
        return

    def __getitem__(self, index):
        return self.net[index]

    def __len__(self):
        return len(self.net)

    def load_weights(self, path):
        input = open(path, 'rb')
        size = array('L')
        size.fromfile(input, 1)
        size = size[0]
        for i in range(size):
            self.net += [weight()]
            self.net[-1].load(input)
        # print(f"Load weight table from '{path}' successfully")
        # print(f"len = {len(self.net)} * {len(self.net[-1])}", end='\n\n')
        return

    def save_weights(self, path):
        output = open(path, 'wb')
        array('L', [len(self.net)]).tofile(output)
        for w in self.net:
            w.save(output)
        # print(f"Save weight table to '{path}' successfully")
        # print(f"len = {len(self.net)} * {len(self.net[-1])}", end='\n\n')
        return

class rndenv(random_agent):
    """
    random environment
    add a new random tile to an empty cell with bagging rule
    """

    def __init__(self, options = ""):
        super().__init__("name=random role=environment " + options)
        self.bag = [] # [zr]
        return

    # [zr] Threes
    def take_action(self, state):
        empty = [pos for pos, tile in enumerate(state.state) if tile == -1]
        if empty:
            pos = self.choice(empty)
            for index in empty:
                state.state[index] = 0
            if not self.bag:
                self.bag = [1, 2, 3]
            tile = self.choice(self.bag)
            self.bag.remove(tile)
            return action.place(pos, tile)
        else:
            return action()

    # [zr] game init
    def init_move(self, state):
        self.bag = init_moves = []
        empty_pos = [i for i in range(16)]
        for i in range(9):
            pos = self.choice(empty_pos)
            empty_pos.remove(pos)
            if not self.bag:
                self.bag = [1, 2, 3]
            tile = self.choice(self.bag)
            self.bag.remove(tile)
            init_moves += [action.place(pos, tile)]
        return init_moves

class player(random_agent):
    """
    dummy player
    select a legal action with TD(0) & n-tuple network
    """

    def __init__(self, options = ""):
        super().__init__("name=dummy role=player " + options)
        # init learning rate
        alpha = self.property("alpha")
        self.alpha = (0.0025 * 17) if alpha == None else float(alpha)
        # init tuple list
        self.tuple_list = [[ 0,  1,  2,  3], # 4 rows
                           [ 4,  5,  6,  7],
                           [ 8,  9, 10, 11],
                           [12, 13, 14, 15],
                           [ 0,  4,  8, 12], # 4 column
                           [ 1,  5,  9, 13],
                           [ 2,  6, 10, 14],
                           [ 3,  7, 11, 15],
                           [ 0,  1,  4,  5], # 9 cube
                           [ 1,  2,  5,  6],
                           [ 2,  3,  6,  7],
                           [ 4,  5,  8,  9],
                           [ 5,  6,  9, 10],
                           [ 6,  7, 10, 11],
                           [ 8,  9, 12, 13],
                           [ 9, 10, 13, 14],
                           [10, 11, 14, 15]]
        self.tuple_count = len(self.tuple_list)
        self.n_tuple = len(self.tuple_list[0])
        # init weight table
        load_path = self.property("load")
        self.w_a = weight_agent(load_path, self.tuple_count, self.n_tuple)
        return
    
    # [zr] save weight table to file
    def __exit__(self, exc_type, exc_value, traceback):
        save_path = self.property("save")
        if save_path is not None:
            self.w_a.save_weights(save_path)
        return

    # [zr] action by max reward
    def take_action(self, state):
        best_op, best_v_s_after = None, None

        # a = argmax_a(evaluate(s))
        for op in range(4):
            b = board(state)
            reward = b.slide(op)
            if reward != -1:
                v_s_after = self.get_board_value(b) + reward
                if best_v_s_after == None or v_s_after > best_v_s_after:
                    best_op = op
                    best_v_s_after = v_s_after

        if best_v_s_after != None:
            return action.slide(best_op)
        else:
            return action()

    # [zr] training function
    def train(self, s_after, s_next = None):
        best_v_s_n_after = None  # td target
        if s_next != None:
            # a_next = argmax_a(evaluate(s_next))
            for op in range(4):
                b = board(s_next)
                reward = b.slide(op)
                if reward != -1:
                    v_s_n_after = self.get_board_value(b) + reward
                    if best_v_s_n_after == None or v_s_n_after > best_v_s_n_after:
                        best_v_s_n_after = v_s_n_after
        if best_v_s_n_after == None:
            best_v_s_n_after = 0
        delta = best_v_s_n_after - self.get_board_value(s_after)

        features = self.get_feature_index(s_after)
        for i in range(self.tuple_count):
            self.w_a[i][features[i]] += (self.alpha / (self.tuple_count * self.n_tuple)) * delta
        return

    # [zr] compute afterstate
    def get_board_value(self, state):
        b_tmp = board(state)
        value = 0
        features = self.get_feature_index(b_tmp)
        for i in range(self.tuple_count):
            value += self.w_a[i][features[i]]
        return value

    # [zr] get features' index of weight table
    def get_feature_index(self, state):
        b_tmp = board(state)

        for i in range(len(b_tmp)):
            # replace -1 by 0
            if b_tmp[i] == -1:
                b_tmp[i] = 0
            # replace tile over 14(6144) by 14
            if b_tmp[i] >= TILE_CONUT:
                b_tmp[i] = TILE_CONUT - 1

        feature_idx = []
        for tuple_pos in self.tuple_list:
            x = 1
            w_table_idx = 0
            for tile_idx in tuple_pos:
                w_table_idx += (b_tmp[tile_idx] * x)
                x *= TILE_CONUT
            feature_idx += [w_table_idx]
        return feature_idx

if __name__ == '__main__':
    print('2048 Demo: agent.py\n')
    state = [0, 1, 2, 3] * 4
    b = board(state)
    p = player()
    print(p.get_feature_index(b))
    pass
