import sys
import os
from datetime import datetime

if __name__ == "__main__":
    start = 0
    times = 10
    unit  =  1
    alpha_half =  0
    
    for para in sys.argv[1:]:
        if "--start=" in para:
            start = int(para[(para.index("=") + 1):])
        elif "--times=" in para:
            times = int(para[(para.index("=") + 1):])
        elif "--unit=" in para:
            unit = int(para[(para.index("=") + 1):])
        elif "--alpha_half=" in para:
            alpha_half = int(para[(para.index("=") + 1):])
    
    alpha = (0.0025 * 17) / (2 ** alpha_half)

    print(f"Auto Training : Start from {start}, train {times} times (unit:{unit}k) with rate = {alpha / 17}", end='\n\n')
    for _ in range(times):
        print('[', datetime.now(), ']', sep='', flush=True)
        if alpha_half == 0:
            if start == 0:
                return_code = os.system(f'python3 Threes.py --total={unit}000 --play="save=w-17_4tuple-{(start+1)*unit}k.bin"')
            else:
                return_code = os.system(f'python3 Threes.py --total={unit}000 --play="load=w-17_4tuple-{start*unit}k.bin save=w-17_4tuple-{(start+1)*unit}k.bin"')
        else:
            if start == 0:
                return_code = os.system(f'python3 Threes.py --total={unit}000 --play="save=w-17_4tuple-{(start+1)*unit}k.bin --alpha={alpha}"')
            else:
                return_code = os.system(f'python3 Threes.py --total={unit}000 --play="load=w-17_4tuple-{start*unit}k.bin save=w-17_4tuple-{(start+1)*unit}k.bin --alpha={alpha}"')
        if return_code == 2: # KeyboardInterrupt
            break
        start += 1
    print('[', datetime.now(), '] End Training!', sep='')